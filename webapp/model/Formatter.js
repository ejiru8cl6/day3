sap.ui.define([], function() {
	"use strict";

	return {
		processStep: function(fValue) {
			var matches = fValue.match(/[^\]@\[]+/g);
			return matches[1];
		},
		
		processApprover: function(fValue){
			var matches = fValue.match(/[^\]@\[]+/g);
			return matches[2];
		},
		
		convertDate: function(fValue){
			var oDate = new Date(fValue);
			return oDate.toLocaleDateString() + " " + (oDate.toLocaleTimeString().slice(0,-3));
		},
		
	};
});