sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nnext/iq/Processing/model/Formatter"
], function(Controller,Formatter) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Show_FormDetail_All", {
		oFormatter: Formatter,
		onInit: function() {
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			$.ajax("/Flow7Api/api/dashboard/processing")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					self.getView().setModel(oData, "processing");
				});
		},
		onListPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("processing");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			if (oItem.DiagramId === "FDP_P0") {
				this._oRouter.navTo("Show_FormItemDetail", {
					requisitionId: oItem.RequisitionId,
					query: {
					}
				});
			}
		}
	});
});