sap.ui.define([
	"sap/ui/core/mvc/Controller",
		"nnext/iq/Processing/model/Formatter"
], function(Controller,Formatter) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Show_FormDetail", {
		oFormatter: Formatter,
		onInit: function() {
			var ctrl = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			$.ajax("/Flow7Api/api/dashboard/processing")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "processing");
				});
			this._oRouter.getRoute("Show_FormDetail").attachMatched(this._handleRouteMatched, this);
		},
		_handleRouteMatched: function(oEvent) {
			this.getView().byId("listPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
			var diagramId = oEvent.getParameter("arguments").diagramId;
			var list = this.getView().byId("list");
			var binding = list.getBinding("items");
			binding.filter(
				[new sap.ui.model.Filter([new sap.ui.model.Filter("DiagramId", sap.ui.model.FilterOperator.EQ, diagramId)]),
					false
				]);
			
		},
		onPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("processing");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			if (oItem.DiagramId === "FDP_P0") {
				this._oRouter.navTo("Show_FormItemDetail", {
					requisitionId: oItem.RequisitionId,
					query: {
					}
				});
			}
		}
	});
});