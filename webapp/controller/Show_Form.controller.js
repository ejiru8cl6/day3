sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Show_Form", {
		onInit: function(oEvent) {
			var ctrl = this;

			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			this._oRouter.attachRouteMatched(function(oEvent) {
				$.ajax("/Flow7Api/api/dashboard/processing/folder")
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
						ctrl.getView().setModel(oData, "folder");
					});
			});
			 this._oRouter.getRoute("Show_Form").attachMatched(this._handleRouteMatched, this);
		},
		_handleRouteMatched: function(oEvent) {
			this.getView().byId("formlistPage").setTitle(oEvent.getParameter("arguments")["?query"].folderName);
		},
		onPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("folder");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			this._oRouter.navTo("Show_FormDetail", {
				diagramId: oItem.Key.DiagramId,
				query: {
					diagramName: oItem.Key.DiagramName
				}
			});
		},
		onBackPress: function(oEvent) {
			this._oRouter.navTo("Show_Folder", {}, true);
			// var oHistory, sPreviousHash;
			// oHistory = History.getInstance();
			// sPreviousHash = oHistory.getPreviousHash();
			// if (sPreviousHash !== undefined) {
			// 	window.history.go(-1);
			// } else {
			// 	this.getRouter().navTo("Show_Folder", {}, true /*no history*/ );
			// }
		}
	});
});