sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nnext/iq/Processing/model/Formatter",
	"sap/ui/core/routing/History"
], function(Controller, Formatter, History) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Show_FormItemDetail", {
		oFormatter: Formatter,
		onInit: function(oEvent) {
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			//進入後每次Runting 都會跑一次??????
			this._oRouter.attachRouteMatched(function(oEvent) {
				var requisitionId = oEvent.getParameter("arguments").requisitionId;
				$.ajax("/Flow7Api/api/fdp/m/" + requisitionId)
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
						self.getView().setModel(oData, "formDetail");
					});
			});
			// this.getView().byId("listPage").setTitle(oEvent.getParameter("arguments")["?query"].serialId);
		},
		onBackPress: function(oEvent) {
			var oHistory, sPreviousHash;
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("Main", {}, true /*no history*/ );
			}
		}
	});
});